<%--
 * 
 * @author Shengzhao Li
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fun" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>用户注册</title>
</head>
<body>

<div>
    <div class="row">
        <div class="col-md-12">
            <h4>用户注册</h4>
            <form:form commandName="formDto" cssClass="form-horizontal">

                <div class="form-group">
                    <label for="username" class="col-sm-2 control-label">账号</label>

                    <div class="col-sm-8">
                        <form:input path="username" id="username" cssClass="form-control"
                                    placeholder="请输入账号" required="true" maxlength="40"/>
                        <p class="help-block">账号必须唯一</p>
                        <form:errors path="username" cssClass="text-danger"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">密码</label>

                    <div class="col-sm-8">
                        <form:password path="password" id="password" cssClass="form-control"
                                       placeholder="请输入密码, 长度 >= 6" required="true" maxlength="255"/>
                        <p class="help-block">密码长度 >= 6</p>
                        <form:errors path="password" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rePassword" class="col-sm-2 control-label">重复密码</label>

                    <div class="col-sm-8">
                        <form:password path="rePassword" id="rePassword" cssClass="form-control"
                                       placeholder="请再次输入密码" required="true" maxlength="255"/>
                        <p class="help-block">两次输入的密码要一致</p>
                        <form:errors path="rePassword" cssClass="text-danger"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">邮箱</label>

                    <div class="col-sm-8">
                        <form:input path="email" id="email" cssClass="form-control"
                                    placeholder="请输入邮箱" maxlength="255" required="true"/>
                        <p class="help-block">邮箱是必填项</p>
                        <form:errors path="email" cssClass="text-danger"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">手机</label>

                    <div class="col-sm-8">
                        <form:input path="phone" id="phone" cssClass="form-control"
                                    placeholder="请输入手机号码" maxlength="255"/>
                        <p class="help-block">手机号码是可选的</p>
                        <form:errors path="phone" cssClass="text-danger"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="captcha" class="col-sm-2 control-label">验证码</label>

                    <div class="col-sm-8">
                        <div class="input-group">
                            <input type="text" name="${formDto.captchaKey}" class="form-control"
                                   placeholder="请输入右侧验证码" id="captcha" maxlength="10" required="required"/>
                            <span class="input-group-addon input-sm">
                                <img src="${contextPath}/captcha.hb" onclick="this.src = this.src+'?'" alt="Captcha"/>
                            </span>
                        </div>
                        <p class="help-block">验证码是必填项</p>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"><em class="fui-check-circle"></em> 注册
                        </button>
                        &nbsp;<a href="${contextPath}/login.hb" class="btn btn-sm btn-link">去登录</a>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

</body>
</html>